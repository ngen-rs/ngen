pub type Vec2 = cgmath::Vector2<f32>;
pub type Vec3 = cgmath::Vector3<f32>;
pub type Vec4 = cgmath::Vector4<f32>;

pub type Vec2i = cgmath::Vector2<i32>;
pub type Vec3i = cgmath::Vector3<i32>;
pub type Vec4i = cgmath::Vector4<i32>;

pub type Vec2u = cgmath::Vector2<u32>;
pub type Vec3u = cgmath::Vector3<u32>;
pub type Vec4u = cgmath::Vector4<u32>;

pub type P2 = cgmath::Point2<f32>;
pub type P3 = cgmath::Point3<f32>;

pub use cgmath::prelude::SquareMatrix;
pub type Mat4 = cgmath::Matrix4<f32>;
