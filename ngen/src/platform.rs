use sdl2::{image::LoadSurface, surface::Surface};

use crate::{arena::TempArena, assets::Bitmap, renderer::OpenGLRenderer};

pub struct Platform<'p> {
    renderer: &'p mut OpenGLRenderer,
    pub temp_arena: &'p mut TempArena,
}

impl<'p> Platform<'p> {
    pub fn new(renderer: &'p mut OpenGLRenderer, temp_arena: &'p mut TempArena) -> Self {
        Self {
            renderer,
            temp_arena,
        }
    }

    // TODO: We should probably be opinionated here and assume the "assets" folder is where we
    // should look. This would allow us to pass in "myimage.png" instead of "assets/myimage.png".
    /// Used to synchronously load an image from disk and submit it to OpenGL. The resuling struct,
    /// assuming the image exists, contains the OpenGL id along with image dimensions. It does not
    /// contain actual image data at this point. In the future, we may change the API to allow the
    /// user to modify images sent to the GPU, but at this point we don't.
    pub fn load_image(&mut self, path: &str) -> Result<Bitmap, String> {
        let target_format = sdl2::pixels::PixelFormatEnum::RGBA8888;
        let spritesheet = Surface::from_file(path)?.convert_format(target_format)?;

        Ok(spritesheet.with_lock(|bytes| unsafe {
            let ptr = bytes.as_ptr().cast::<u32>();
            let size = bytes.len() / 4;
            let slice = std::slice::from_raw_parts(ptr, size);
            let (width, height) = (spritesheet.width(), spritesheet.height());
            let texture_id = self.renderer.load_texture(slice, width, height);
            Bitmap::new(texture_id, width as f32, height as f32)
        }))
    }
}
