# ngen

A _very_ simple game engine written in Rust using OpenGL. This is largely a toy, but if you can build an application off of it, by all means do!

