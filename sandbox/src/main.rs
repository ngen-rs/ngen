#![warn(rust_2018_idioms)]

use ngen::{assets::TiledBitmap, math::Vec2, Camera, Input, Platform, RenderCommands, Tick};

const MAX_QUADS: u32 = 25_000;

struct Game {
    pos: Vec2,
    image: Option<TiledBitmap>,
    camera: Camera,
}

impl Default for Game {
    // We're implementing this manually to demonstrate how we would go about using the
    // `mount_default` call when we're building up the platform settings.
    fn default() -> Self {
        let camera = Camera::from_tiles(30);
        Self {
            pos: Vec2::new(2., 2.),
            image: None,
            camera,
        }
    }
}

impl ngen::Simulation for Game {
    // Our update method is called several times per frame to give a fixed timestep. This method is
    // adapted from the Gaffer on Games article, "Fix Your Timestep!" found at
    // https://www.gafferongames.com/post/fix_your_timestep/
    fn update(
        &mut self,
        platform: &mut Platform<'_>,
        input: &Input,
        tick: Tick,
    ) -> Result<bool, String> {
        if let None = &self.image {
            // We're using Kenney's 1-Bit Platformer Pack for our sandbox.
            // https://kenney.nl/assets/bit-platformer-pack
            let spritesheet = platform.load_image("assets/spritesheet.png")?;
            self.image.replace(spritesheet.as_tiled(20, 20));
        }

        {
            // This is silly, but demonstrates how to use the temp arena to store data for this current
            // tick without requiring a ton of allocations from the system.
            let forty_two = platform.temp_arena.push(42);
            assert_eq!(forty_two, &42);

            let seven = platform.temp_arena.push(7.);
            assert_eq!(seven, &7.);

            if input.keyboard.f[01].was_down() {
                // With the temp arena, we have access to a growable array. Because elements are
                // added sequentially, the array takes exclusive access to the temp arena.
                let mut array = platform.temp_arena.array();
                for i in 0..42 {
                    array.push(i);
                }

                // To regain access to the temp arena, we call `end` on the array, which returns a
                // slice that points to the elements that were pushed onto the array.
                print!("Array: ");
                for i in array.end() {
                    print!("{} ", i);
                }
                println!();

                // If we need to share the temp arena, we can use a linked list, which does not
                // take exclusive access. Because a linked list doesn't require all of its elements
                // to be layed out sequentially in memory, we only pass it to the linked list when
                // we want to push on a new value.
                let mut numbers = ngen::arena::List::new();
                for i in 0..42 {
                    numbers.push(&platform.temp_arena, i);
                }

                // We can `iter_mut` through all of the elements.
                for n in numbers.iter_mut() {
                    *n *= 2;
                }

                // Nothing fancy here, we can iterate all of the values list we would for any Rust
                // collection. Doing so, unlike the `Array`, does not consume the linked list.
                print!("Linked list: ");
                for i in numbers.iter() {
                    print!("{} ", i);
                }
                println!();

                // So, we can still push elements on.
                numbers.push(&platform.temp_arena, 1024);
            }
        }

        if input.controller.up.is_down() {
            self.pos.y += 5. * tick.dt;
        }

        if input.controller.down.is_down() {
            self.pos.y -= 5. * tick.dt;
        }

        if input.controller.left.is_down() {
            self.pos.x -= 5. * tick.dt;
        }

        if input.controller.right.is_down() {
            self.pos.x += 5. * tick.dt;
        }

        if input.controller.action_a.is_down() {
            self.camera -= Vec2::new(5., 0.) * tick.dt;
        }

        if input.controller.action_b.is_down() {
            self.camera += Vec2::new(0., 5.) * tick.dt;
        }

        if input.controller.action_c.is_down() {
            self.camera += Vec2::new(5., 0.) * tick.dt;
        }

        if input.controller.action_d.is_down() {
            self.camera -= Vec2::new(0., 5.) * tick.dt;
        }

        // Exit when escape is hit (for now)
        Ok(!input.keyboard.escape.is_down())
    }

    // Our render method is called once per frame.
    fn render(&mut self, render_commands: &mut RenderCommands) -> Result<(), String> {
        let transform = self.camera.build_transform();

        render_commands.with_render_layer(transform, |layer| {
            layer.clear_screen((0.05, 0.05, 0.05, 0.).into());

            if let Some(spritesheet) = self.image {
                // This renders a sprite to the screen. Here, we're using a sprite from our
                // spritesheet. Here, we could animate the sprite by flipping between the walking
                // animation sprites, if we wanted to.
                layer.push_sprite(
                    self.pos, // The position is the center of the sprite.
                    (1., 1.).into(), // The dimensions of the sprite as we should render them on the screen.
                    (1., 1., 1., 1.).into(), // The color to pass to the shader.
                    spritesheet.get_sprite(0, 12), // The sprite we want to pass in.
                );

                // This renders a recangular solid to the screen.
                layer.push_quad(
                    (1., 1.).into(), // The position is the center of the quad.
                    (1., 1.).into(), // The quad's dimensions.
                    (1., 1., 1., 1.).into(), // And, finally, the quad's color.
                );
            }
        });

        Ok(())
    }
}

fn main() -> Result<(), String> {
    ngen::builder()
        // Describe the window dimensions and title;
        .window(1920, 1080, "ngen Sandbox")
        // and, how many quads we want the engine to allocate space for;
        .max_quads(MAX_QUADS)
        // and, describe the size of the fixed timestep we want to use;
        .dt(0.01)
        // and, what the maximum frame time should be;
        .max_frame(0.1)
        // and, how much temporary memory we should allocate;
        .temp_memory(1024)
        // and, finally, mount the simulation and run it.;
        .mount_default::<Game>()
}
